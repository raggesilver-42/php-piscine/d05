SELECT
  UPPER(u.`last_name`) AS 'NAME',
  u.`first_name`,
  s.price
FROM
  `user_card` u
  JOIN `member` m ON u.id_user = m.id_member
  JOIN `subscription` s ON s.id_sub = m.id_sub
WHERE
  s.price > 42
ORDER BY
  u.last_name ASC,
  u.first_name ASC;
