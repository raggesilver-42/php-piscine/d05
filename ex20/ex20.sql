SELECT
  m.id_genre,
  g.name AS `name_genre`,
  m.id_distrib,
  d.name AS `name_distrib`,
  m.title AS `title_film`
FROM
  `film` m
  LEFT JOIN `genre` g ON g.id_genre = m.id_genre
  LEFT JOIN `distrib` d ON d.id_distrib = m.id_distrib
WHERE
  m.id_genre BETWEEN 4 AND 8;
